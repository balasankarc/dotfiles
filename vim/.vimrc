"-------------------------
"Specify necessary plugins
"-------------------------
call plug#begin('~/.vim/plugged')

Plug 'https://github.com/Raimondi/delimitMate.git'
Plug 'https://github.com/bogado/file-line.git', { 'branch': 'main' }
Plug 'https://github.com/vim-scripts/HTML-AutoCloseTag.git'
Plug 'https://github.com/Yggdroot/indentLine.git'
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --key-bindings --completion --no-update-rc' }
Plug 'junegunn/fzf.vim'
Plug 'https://github.com/scrooloose/nerdtree.git'
Plug 'dense-analysis/ale'
Plug 'shumphrey/fugitive-gitlab.vim'
Plug 'https://github.com/vim-airline/vim-airline.git'
Plug 'https://github.com/tpope/vim-endwise.git'
Plug 'https://github.com/tpope/vim-fugitive.git'
Plug 'https://github.com/tpope/vim-surround.git'
Plug 'https://github.com/tpope/vim-dispatch.git'
Plug 'https://github.com/tpope/vim-commentary.git'
Plug 'https://github.com/airblade/vim-gitgutter.git', { 'branch': 'main' }
Plug 'https://github.com/tmhedberg/matchit.git'
Plug 'christoomey/vim-tmux-navigator'
Plug 'godlygeek/tabular'
Plug 'thoughtbot/vim-rspec'
Plug 'ryanoasis/vim-devicons'
Plug 'nsf/gocode', { 'rtp': 'vim', 'do': '~/.vim/plugged/gocode/vim/symlink.sh' }
Plug 'ChartaDev/charta.vim'
Plug 'jamessan/vim-gnupg', { 'branch': 'main' }
Plug 'tpope/vim-projectionist'
Plug 'tpope/vim-dispatch'
Plug 'thoughtbot/vim-rspec'
Plug 'altercation/vim-colors-solarized'
Plug 'posva/vim-vue'
Plug 'tpope/vim-rails'
Plug 'nvim-lua/plenary.nvim'
Plug 'antoinemadec/FixCursorHold.nvim'
Plug 'nvim-neotest/neotest'
Plug 'olimorris/neotest-rspec', { 'branch': 'main' }
Plug 'mortepau/codicons.nvim'
Plug 'neovim/nvim-lspconfig'
Plug 'ray-x/go.nvim'
Plug 'hrsh7th/nvim-compe'
Plug 'hrsh7th/vim-vsnip'
Plug 'nvim-treesitter/nvim-treesitter'
call plug#end()

"----------------
"Startup settings
"----------------


"Use 256 colours
set t_Co=256

"Enable filetype detection, plugin identification and auto indentation
filetype plugin indent on

"Use System Clipboard
set clipboard=unnamedplus
vmap <expr>  ++  VMATH_YankAndAnalyse()
nmap         ++  vip++

"Displaying Line Numbers
:set number

"Highlight search matches
set hlsearch

"Set incremental search. n and N to iterate
set incsearch

"Show cursor line
set cursorline

"Enable wildchar based matching on command
set wildmenu

"Always show status line
set laststatus=2

"Automatic vimrc reload
autocmd! bufwritepost ~/.vimrc source %

"Show auto-completion matches
set completeopt=menuone,noselect
"longest,preview

"Expanding tab to spaces
set expandtab

"Persistent Undo
set undofile
set undodir=~/.vim/undodir
set undolevels=1000
set undoreload=10000

"Set swap directory
set directory=~/.vim/swp/

"Set tab width
set ts=2 sts=2 sw=2

"Don't conceal on JSON and Markdown
let g:vim_json_conceal=0
let g:markdown_syntax_conceal=0

"Set text width
autocmd Filetype markdown setlocal textwidth=80
autocmd Filetype md setlocal textwidth=80

"Show 2 lines before and after curent line
set scrolloff=2

"-----------------
"Keyboard Bindings
"-----------------

"Intendation
nnoremap <Tab> >>_
nnoremap <S-Tab> <<_
inoremap <S-Tab> <C-D>
vnoremap <Tab> >gv
vnoremap <S-Tab> <gv

"Mapping ctrl-t to open new tab
:map <C-n> :tabnew<CR>
:imap <C-n> <ESC><C-n>

"Mapping ctrl-shift-right to move to next tab
:map <C-l> :tabnext<CR>
:imap <C-l> <ESC><C-l>

"Mapping ctrl-shift-left to move to previous tab
:map <C-h> :tabprevious<CR>
:imap <C-h> <ESC><C-h>

"Mapping ctrl-a to select all
:map <C-a> GVgg
:imap <C-a> <ESC><C-a>
:nnoremap <C-a> GVgg

"Paste as raw text
set pastetoggle=<F2>

"Map NerdTree to Control-F
let NERDTreeChDirMode=1
nnoremap <leader>n :NERDTree .<CR>
noremap <C-F> :NERDTreeToggle<CR>

"Setting <F5> to run python/ruby script
autocmd FileType python nmap <F5> :!python %<CR>
autocmd FileType ruby nmap <F5> :!ruby %<CR>

"sudo prompt on system files
cmap w!! w !sudo tee >/dev/null %

"Map ctrl-p to FZF
:map <C-p> :ProjectFiles<CR>
"":map <C-p> :find 

"Map jk to esc
imap jk <ESC>

"Map for git blame
:vmap <Leader>b :<C-U>!git blame <C-R>=expand("%:p") <CR> \| sed -n <C-R>=line("'<") <CR>,<C-R>=line("'>") <CR>p <CR>

"Trailing spaces
set list listchars=tab:».,trail:.

"Mark column 80
let &colorcolumn=80
autocmd ColorScheme zellner highlight ColorColumn ctermbg=0

set shiftround

let g:ycm_seed_identifiers_with_syntax = 1

" Ignore git and bundle folders while searching
let $FZF_DEFAULT_COMMAND='fdfind --hidden --exclude ".git" --exclude ".bundle" --strip-cwd-prefix'

" Make FZF search from git root only
function! s:find_git_root()
  return system('git rev-parse --show-toplevel 2> /dev/null')[:-2]
endfunction
command! ProjectFiles execute 'Files' s:find_git_root()

set path+=**
set wildmenu

set splitright
" Open tag in split and tab
map <C-\> :tab split<CR>:exec("tag ".expand("<cword>"))<CR>
map <A-]> :vsp <CR>:exec("tag ".expand("<cword>"))<CR>

" Map tmu-navigator to match tmux conf
let g:tmux_navigator_no_mappings = 1
nnoremap <silent> <A-h> :TmuxNavigateLeft<CR>
nnoremap <silent> <A-j> :TmuxNavigateDown<CR>
nnoremap <silent> <A-k> :TmuxNavigateUp<CR>
nnoremap <silent> <A-l> :TmuxNavigateRight<CR>
nnoremap <silent> <A-p> :TmuxNavigatePrevious<CR>

if has("autocmd")
  " Enable filetype detection
  filetype plugin indent on

  " Restore cursor position
  autocmd BufReadPost *
    \ if line("'\"") > 1 && line("'\"") <= line("$") |
    \   exe "normal! g`\"" |
    \ endif
endif
if &t_Co > 2 || has("gui_running")
  " Enable syntax highlighting
  syntax on
endif

" Do not run ale for Ruby
let g:ale_linters = {'ruby': ['solargraph']}

" Set the executable for ALE to call to get Solargraph
" up and running in a given session
let g:ale_ruby_solargraph_executable = 'solargraph'
let g:ale_ruby_solargraph_options = {}

let g:ale_completion_enabled = 1
"Run ale only on save
let g:ale_lint_on_text_changed = 'never'
let g:ale_lint_on_insert_leave = 0
let g:ale_lint_on_enter = 0
let g:ale_sign_error                  = '✘'
let g:ale_sign_warning                = '⚠'
highlight ALEErrorSign ctermbg        =NONE ctermfg=red
highlight ALEWarningSign ctermbg      =NONE ctermfg=yellow
highlight ALEWarning ctermbg=LightGray ctermfg=black
highlight ALEError ctermbg=LightGray ctermfg=black

nnoremap <leader>r :Dispatch!<CR>


lua << EOF
local lspconfig = require("lspconfig")
lspconfig.gopls.setup({
  settings = {
    gopls = {
      analyses = {
        unusedparams = true,
      },
      staticcheck = true,
      gofumpt = true,
    },
  },
})
require'lspconfig'.solargraph.setup{}

local nvim_lsp = require('lspconfig')

-- Use an on_attach function to only map the following keys
-- after the language server attaches to the current buffer
local on_attach = function(client, bufnr)
  local function buf_set_keymap(...) vim.api.nvim_buf_set_keymap(bufnr, ...) end
  local function buf_set_option(...) vim.api.nvim_buf_set_option(bufnr, ...) end

  --Enable completion triggered by <c-x><c-o>
  buf_set_option('omnifunc', 'v:lua.vim.lsp.omnifunc')

  -- Mappings.
  local opts = { noremap=true, silent=true }

  -- See `:help vim.lsp.*` for documentation on any of the below functions
  buf_set_keymap('n', 'gD', '<Cmd>lua vim.lsp.buf.declaration()<CR>', opts)
  buf_set_keymap('n', 'gd', '<Cmd>lua vim.lsp.buf.definition()<CR>', opts)
  buf_set_keymap('n', 'K', '<Cmd>lua vim.lsp.buf.hover()<CR>', opts)
  buf_set_keymap('n', 'gi', '<cmd>lua vim.lsp.buf.implementation()<CR>', opts)
  buf_set_keymap('n', '<C-k>', '<cmd>lua vim.lsp.buf.signature_help()<CR>', opts)
  buf_set_keymap('n', '<space>wa', '<cmd>lua vim.lsp.buf.add_workspace_folder()<CR>', opts)
  buf_set_keymap('n', '<space>wr', '<cmd>lua vim.lsp.buf.remove_workspace_folder()<CR>', opts)
  buf_set_keymap('n', '<space>wl', '<cmd>lua print(vim.inspect(vim.lsp.buf.list_workspace_folders()))<CR>', opts)
  buf_set_keymap('n', '<space>D', '<cmd>lua vim.lsp.buf.type_definition()<CR>', opts)
  buf_set_keymap('n', '<space>rn', '<cmd>lua vim.lsp.buf.rename()<CR>', opts)
  buf_set_keymap('n', '<space>ca', '<cmd>lua vim.lsp.buf.code_action()<CR>', opts)
  buf_set_keymap('n', 'gr', '<cmd>lua vim.lsp.buf.references()<CR>', opts)
  buf_set_keymap('n', '<space>e', '<cmd>lua vim.lsp.diagnostic.show_line_diagnostics()<CR>', opts)
  buf_set_keymap('n', '[d', '<cmd>lua vim.lsp.diagnostic.goto_prev()<CR>', opts)
  buf_set_keymap('n', ']d', '<cmd>lua vim.lsp.diagnostic.goto_next()<CR>', opts)
  buf_set_keymap('n', '<space>q', '<cmd>lua vim.lsp.diagnostic.set_loclist()<CR>', opts)
  buf_set_keymap("n", "<space>f", "<cmd>lua vim.lsp.buf.formatting()<CR>", opts)

end

-- Use a loop to conveniently call 'setup' on multiple servers and
-- map buffer local keybindings when the language server attaches
local servers = { "solargraph" }
for _, lsp in ipairs(servers) do
  nvim_lsp[lsp].setup {
    on_attach = on_attach,
    flags = {
      debounce_text_changes = 150,
    }
}
end
EOF


lua << EOF
vim.o.completeopt = "menuone,noselect"

require'compe'.setup {
  enabled = true;
  autocomplete = true;
  debug = false;
  min_length = 1;
  preselect = 'enable';
  throttle_time = 80;
  source_timeout = 200;
  incomplete_delay = 400;
  max_abbr_width = 100;
  max_kind_width = 100;
  max_menu_width = 100;
  documentation = false;

  source = {
    path = true;
    buffer = true;
    calc = true;
    vsnip = true;
    nvim_lsp = true;
    nvim_lua = true;
    spell = true;
    tags = true;
    snippets_nvim = true;
    treesitter = true;
  };
}
local t = function(str)
  return vim.api.nvim_replace_termcodes(str, true, true, true)
end

local check_back_space = function()
    local col = vim.fn.col('.') - 1
    if col == 0 or vim.fn.getline('.'):sub(col, col):match('%s') then
        return true
    else
        return false
    end
end

-- Use (s-)tab to:
--- move to prev/next item in completion menuone
--- jump to prev/next snippet's placeholder
_G.tab_complete = function()
  if vim.fn.pumvisible() == 1 then
    return t "<C-n>"
  elseif vim.fn.call("vsnip#available", {1}) == 1 then
    return t "<Plug>(vsnip-expand-or-jump)"
  elseif check_back_space() then
    return t "<Tab>"
  else
    return vim.fn['compe#complete']()
  end
end
_G.s_tab_complete = function()
  if vim.fn.pumvisible() == 1 then
    return t "<C-p>"
  elseif vim.fn.call("vsnip#jumpable", {-1}) == 1 then
    return t "<Plug>(vsnip-jump-prev)"
  else
    -- If <S-Tab> is not working in your terminal, change it to <C-h>
    return t "<S-Tab>"
  end
end

vim.api.nvim_set_keymap("i", "<Tab>", "v:lua.tab_complete()", {expr = true})
vim.api.nvim_set_keymap("s", "<Tab>", "v:lua.tab_complete()", {expr = true})
vim.api.nvim_set_keymap("i", "<S-Tab>", "v:lua.s_tab_complete()", {expr = true})
vim.api.nvim_set_keymap("s", "<S-Tab>", "v:lua.s_tab_complete()", {expr = true})
EOF

set number

augroup numbertoggle
  autocmd!
  autocmd BufEnter,FocusGained,InsertLeave,WinEnter * if &nu && mode() != "i" | set rnu   | endif
  autocmd BufLeave,FocusLost,InsertEnter,WinLeave   * if &nu                  | set nornu | endif
augroup END

imap <C-c> <Esc>
