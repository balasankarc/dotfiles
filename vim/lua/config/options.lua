vim.g.mapleader = " "

local opt = vim.opt
---- Spaces, Tabs, Indentation
opt.tabstop = 2
opt.shiftwidth = 2
opt.softtabstop = 2
opt.expandtab = true
opt.smartindent = true
opt.wrap = false

---- Search
opt.incsearch = true -- Incremental search
opt.ignorecase = true -- Ignore case
opt.smartcase = true -- When capitalization is used, stop ignoring case
opt.hlsearch = true -- Highlight all instances of search

---- Numbering
opt.number = true
opt.relativenumber = true
opt.scrolloff = 5

---- Behaviour
opt.hidden = true
opt.errorbells = false
opt.undodir = vim.fn.expand("~/.vim/undodir")
opt.undofile = true
opt.undolevels = 1000
opt.undoreload = 10000
opt.backspace = "indent,eol,start"
opt.splitright = true
opt.splitbelow = true
opt.autochdir = false -- Don't change cwd to directory of current file
opt.iskeyword:append("-") -- When deleting word, include hyphens as part of word
opt.mouse="" -- Disable mouse
opt.clipboard = 'unnamedplus' -- Use system clipboard
opt.modifiable = true
opt.wildmenu = true
opt.shiftround = true
opt.termguicolors = true
opt.completeopt = "menu,menuone,noinsert,noselect"

vim.g.nvim_json_conceal = 0
vim.g.markdown_syntax_conceal = 0

local augroup = vim.api.nvim_create_augroup   -- Create/get autocommand group
local autocmd = vim.api.nvim_create_autocmd   -- Create autocommand

--- Set relativenumber only on focus and not on insert mode
augroup("numbertoggle", {})
autocmd({"BufEnter", "FocusGained", "InsertLeave"}, {
  group = "numbertoggle",
  pattern = "*",
  callback = function ()
    opt.relativenumber = true
  end
})
autocmd({"BufLeave", "FocusLost", "InsertEnter"}, {
  group = "numbertoggle",
  pattern = "*",
  callback = function ()
    opt.relativenumber = false
  end
})

--- Open file on the line where it was closed
autocmd("BufReadPost", {
  pattern = "*",
  callback = function ()
    if vim.fn.line("'\"") > 1 and vim.fn.line("'\"") <= vim.fn.line("$") then
      vim.api.nvim_exec("normal! g'\"",false)
    end
  end
})

---- Set colorcolumn only for git commit messages
autocmd("FileType", {
  pattern = "gitcommit",
  callback = function ()
    opt.colorcolumn = '80'
  end
})

autocmd("BufEnter", {
  pattern = "*_spec.rb",
  callback = function ()
    require("neotest").summary.open()
  end
})

autocmd("BufEnter", {
    pattern = "*Neotest Summary*",
    callback = function()
        local windows = vim.fn.winnr('$')
        if windows == 1 then
            vim.cmd('quit')  -- Close Neovim if it's the last window
        end
    end,
})

-- --- Run rubocop on autosave
-- autocmd("BufWritePre", {
--   pattern = {"*.rb", "*.rake"},
--   callback = function()
--     vim.lsp.buf.format()
--   end,
-- })

-- Configure command used by FZF
vim.env.FZF_DEFAULT_COMMAND='fdfind --hidden --exclude ".git" --exclude ".bundle" --strip-cwd-prefix'


-- Show Rubocop cop name in virtual text
vim.diagnostic.config({
  virtual_text = {
    format = function(value)
      return string.format('[%s] %s', value.code, value.message)
    end,
  },
})
