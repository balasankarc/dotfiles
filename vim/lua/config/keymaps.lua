-- Tab key behavior
vim.keymap.set('n', '<Tab>', '>>_', { noremap = true } )
vim.keymap.set('n', '<S-Tab>', '<<_', { noremap = true } )
vim.keymap.set('i', '<S-Tab>', '<C-D>', { noremap = true } )
vim.keymap.set('v', '<Tab>', '>gv', { noremap = true } )
vim.keymap.set('v', '<S-Tab>', '<gv', { noremap = true } )

-- Tabs
vim.keymap.set('n', '<C-n>', ':tabnew<CR>', { noremap = true } )
vim.keymap.set('i', '<C-n>', '<ESC><C-n>', { noremap = true } )
vim.keymap.set('n', '<C-l>', ':tabnext<CR>', { noremap = true } )
vim.keymap.set('i', '<C-l>', '<ESC><C-l>', { noremap = true } )
vim.keymap.set('n', '<C-h>', ':tabprevious<CR>', { noremap = true } )
vim.keymap.set('i', '<C-h>', '<ESC><C-h>', { noremap = true } )

-- Select all
vim.keymap.set('n', '<C-a>', 'GVgg', { noremap = true } )
vim.keymap.set('i', '<C-n>', '<ESC><C-a>', { noremap = true } )

-- JK to Esc
vim.keymap.set('i', 'jk', '<ESC>', { noremap = true } )

-- Ctrl-C to Esc
vim.keymap.set('i', '<C-c>', '<ESC>', { noremap = true } )
