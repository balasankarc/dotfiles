return {
  "alexghergh/nvim-tmux-navigation",
  opts = {
    disable_when_zoomed = true
  },
  keys = {
    { "<A-h>",function () require('nvim-tmux-navigation').NvimTmuxNavigateLeft() end, mode = "n" },
    { "<A-j>", function () require('nvim-tmux-navigation').NvimTmuxNavigateDown() end, mode = "n" },
    { "<A-k>", function () require('nvim-tmux-navigation').NvimTmuxNavigateUp() end, mode = "n" },
    { "<A-l>", function () require('nvim-tmux-navigation').NvimTmuxNavigateRight() end, mode = "n" },
    { "<A-\\>", function () require('nvim-tmux-navigation').NvimTmuxNavigateLastActive() end, mode = "n" },
    { "<A-Space>", function () require('nvim-tmux-navigation').NvimTmuxNavigateNext() end, mode = "n" },
  },
}
