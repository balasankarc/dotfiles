return {
  {
    "ibhagwan/fzf-lua",
    keys = {
      {
        "<C-p>", function ()
          local output = vim.fn.system("git rev-parse --show-toplevel 2> /dev/null")
          if output == nil or output == ''
            then
              require('fzf-lua').files()
            else
              require('fzf-lua').git_files({ cmd = "git ls-files --cached --others" })
          end
        end, mode = "n"
      }
    }
  }
}
