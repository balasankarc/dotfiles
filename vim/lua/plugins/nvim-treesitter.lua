return {
  {
    "nvim-treesitter/nvim-treesitter",
    config = function ()
      ensure_installed = { "ruby" }
    end
  }
}
