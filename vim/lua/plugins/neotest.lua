return {
  {
    "nvim-neotest/neotest",
    dependencies = {
      "nvim-neotest/nvim-nio",
      "nvim-lua/plenary.nvim",
      "antoinemadec/FixCursorHold.nvim",
      "nvim-treesitter/nvim-treesitter",
      "olimorris/neotest-rspec"
    },
    config = function ()
      require("neotest").setup({
        output = {
          enabled = true,
          open_on_run = "short"
        },
        output_panel = {
          enabled = true,
        },
        floating = {
          border = "rounded",
        },
        status = {
          virtual_text = false,
        },
        jump = {
          enabled = true,
        },
        adapters = {
          require("neotest-rspec") ({
            filter_dirs = { "worktrees", "gems", "vendor" }
          })
        },
      })
    end,
    keys = {
      { '<leader>tt', function() require("neotest").run.run() end, mode='n' },
      { '<leader>tT', function() require("neotest").run.run(vim.fn.expand("%")) end, mode='n' },
      { '<leader>to', function() require("neotest").output.open({ enter = true, auto_close = true}) end, mode='n' },
      { '<leader>tO', function() require("neotest").output_panel.toggle() end, mode='n' },
      { '<leader>ts', function() require("neotest").summary.toggle() end, mode='n' }
    }
  },
}
